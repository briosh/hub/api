package errors

import (
	"fmt"
	"net/http"
)

var (
	ErrBadRequest       = Error{Code: 9400, Message: "Bad request", HTTP: http.StatusBadRequest}
	ErrNotFound         = Error{Code: 9404, Message: "Not found", HTTP: http.StatusNotFound}
	ErrMethodNotAllowed = Error{Code: 9405, Message: "Method Not Allowed", HTTP: http.StatusMethodNotAllowed}
	ErrNotImplemented   = Error{Code: 9501, Message: "Not implemented", HTTP: http.StatusNotImplemented}
)

type Error struct {
	Code    int    `json:"code" example:"9404"`
	Message string `json:"message" example:"Not found"`
	HTTP    int    `json:"http,omitempty" example:"404"`
}

func (e Error) Error() string {
	return fmt.Sprintf("[%d] %s", e.Code, e.Message)
}
