package errors

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Handler(err error, ctx echo.Context) {
	var res = Error{Code: -1, Message: "Internal error", HTTP: http.StatusInternalServerError}

	switch err.(type) {
	case Error:
		_ = ctx.JSON(res.HTTP, err)
		return
	}

	switch err {
	case echo.ErrMethodNotAllowed:
		res = ErrMethodNotAllowed
	case echo.ErrNotFound:
		res = ErrNotFound
	}

	_ = ctx.JSON(res.HTTP, res)
}
