package ip

import (
	"math/big"
	"net"
)

func Discover(ip, cidr string) (Discovered, error) {
	// Parse IP address with CIDR netmask
	_, network, err := net.ParseCIDR(ip + "/" + cidr)
	if err != nil {
		return Discovered{}, err
	}

	// Prepare discovered results
	discovered := Discovered{Address: IP(network.IP), Netmask: IP(network.Mask)}

	// Compute netmask wildcard
	discovered.Wildcard = make(IP, len(network.Mask))
	for i := 0; i < len(network.Mask); i++ {
		discovered.Wildcard[i] = network.Mask[i] ^ 0xff
	}

	// Compute netmask broadcast
	discovered.Broadcast = make(IP, len(discovered.Address))
	for i := 0; i < len(discovered.Address); i++ {
		discovered.Broadcast[i] = discovered.Address[i] | discovered.Wildcard[i]
	}

	// Compute netmask first ip
	discovered.FirstIP = make(IP, len(discovered.Address), len(discovered.Address))
	copy(discovered.FirstIP, discovered.Address)
	discovered.FirstIP[len(discovered.FirstIP)-1]++

	// Compute netmask last ip
	discovered.LastIP = make(IP, len(discovered.Broadcast), len(discovered.Broadcast))
	copy(discovered.LastIP, discovered.Broadcast)
	discovered.LastIP[len(discovered.LastIP)-1]--

	// Compute total ip address available
	discovered.TotalIP = ip2Int(discovered.Broadcast).Uint64() - ip2Int(discovered.Address).Uint64() - 1

	// Disable results based of netmask
	if ones, bits := network.Mask.Size(); ones == bits {
		discovered.Broadcast = nil
		discovered.FirstIP = nil
		discovered.LastIP = nil
		discovered.TotalIP = 0
	} else if (ones + 1) == bits {
		discovered.FirstIP = discovered.Address
		discovered.LastIP = discovered.Broadcast
		discovered.Address = nil
		discovered.Broadcast = nil
	}

	return discovered, nil
}

type Discovered struct {
	Address   IP
	Broadcast IP
	Netmask   IP
	Wildcard  IP
	FirstIP   IP
	LastIP    IP
	TotalIP   uint64
}

type IP net.IP

func (ip IP) String() string {
	if ip == nil {
		return ""
	}

	return net.IP(ip).String()
}

func ip2Int(ip IP) *big.Int {
	i := big.NewInt(0)
	i.SetBytes(ip)
	return i
}
