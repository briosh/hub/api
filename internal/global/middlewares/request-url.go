package middlewares

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/evolves-fr/gommon"
)

func RequestURL() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			if gommon.Empty(ctx.Request().URL.Scheme) {
				ctx.Request().URL.Scheme = gommon.Ternary(ctx.Request().TLS == nil, "http", "https")
			}

			if gommon.Empty(ctx.Request().URL.Host) {
				ctx.Request().URL.Host = ctx.Request().Host
			}

			return next(ctx)
		}
	}
}
