package internal

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/briosh/hub/api/internal/global/errors"
	"gitlab.com/briosh/hub/api/internal/global/middlewares"
	"gitlab.com/briosh/hub/api/internal/v1"
	"gitlab.com/evolves-fr/gommon"
	"golang.org/x/crypto/acme/autocert"
)

func New(address string) error {
	router := echo.New()
	router.HideBanner = true
	router.HTTPErrorHandler = errors.Handler
	router.AutoTLSManager = autocert.Manager{
		Prompt: autocert.AcceptTOS,
		Cache:  autocert.DirCache("./certs"),
		HostPolicy: func(ctx context.Context, host string) error {
			if gommon.Has(host, "hub.briosh.dev") {
				return nil
			}
			return fmt.Errorf("invalid host '%s'", host)
		},
		Email: "contact@briosh.dev",
	}

	router.Use(middleware.Recover())
	router.Use(middleware.Logger())
	router.Use(middlewares.RequestURL())

	if err := v1.New(router.Group("/v1")); err != nil {
		return err
	}

	for _, route := range router.Routes() {
		logrus.Debugf("[%10s] %s", route.Method, route.Path)
	}

	if strings.HasSuffix(address, ":443") {
		return router.StartAutoTLS(address)
	} else {
		return router.Start(address)
	}
}
