package v1

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/global/info"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/briosh/hub/api/internal/v1/controllers"
	"gitlab.com/briosh/hub/api/internal/v1/docs"
	"gitlab.com/briosh/hub/api/internal/v1/middlewares"
	"pkg.brio.sh/swaggerui/v4"
)

// New hub API
// @title           			Hub API
// @description     			The API toolbox, contains all the resources you may need in your daily life.
// @basePath  					/v1
// @contact.name   				Support
// @contact.url    				https://dev.brio.sh
// @contact.email  				support@brio.sh
// @tag.name 					Whoami
// @tag.description 			Retrieves all information of request
// @tag.name 					IP
// @tag.description 			Retrieves all information for IP address with geolocation
// @tag.name 					Images
// @tag.description				Transform images
// @tag.name 					Tools
// @tag.description				Swiss army knife for developers
// @tag.name 					Fake
// @tag.description 			API for testing and prototyping
func New(router *echo.Group) error {
	core, err := context.NewCore()
	if err != nil {
		return err
	}

	docs.SwaggerInfo.Version = info.Version

	router.Use(middlewares.Context(core))

	router.GET("/doc.json", controllers.DocResource.Swagger)
	router.GET("/doc/*", echo.WrapHandler(http.StripPrefix("/v1/doc/", swaggerui.Handler("/v1/doc.json"))))

	router.Any("/whoami", controllers.WhoamiResources{}.Any)

	ip := router.Group("/ip")
	{
		ip.GET("/geo", controllers.IPResource{}.GeoMe)
		ip.GET("/geo/:ip", controllers.IPResource{}.GeoSearch)
		ip.GET("/compute/:ip/:mask", controllers.IPResource{}.Compute)
	}

	images := router.Group("/images")
	{
		images.POST("/convert", controllers.ImageResource.Convert)
		images.GET("/placeholder/:size", controllers.ImageResource.Placeholder)
	}

	tools := router.Group("/tools")
	{
		tools.POST("/hash", controllers.ToolsResource.Hash)
		tools.POST("/number", controllers.ToolsResource.Number)
	}

	fake := router.Group("/fake")
	{
		// Users
		fake.GET("/users", controllers.FakeResource.Users().List)
		fake.POST("/users", controllers.FakeResource.Users().Create)
		fake.GET("/users/:id", controllers.FakeResource.Users().Show)
		fake.PUT("/users/:id", controllers.FakeResource.Users().Update)
		fake.PATCH("/users/:id", controllers.FakeResource.Users().Patch)
		fake.DELETE("/users/:id", controllers.FakeResource.Users().Delete)
		fake.GET("/users/:id/albums", controllers.FakeResource.Users().Albums)
		fake.GET("/users/:id/todos", controllers.FakeResource.Users().Todos)
		fake.GET("/users/:id/posts", controllers.FakeResource.Users().Posts)

		// Todos
		fake.GET("/todos", controllers.FakeResource.Todos().List)
		fake.POST("/todos", controllers.FakeResource.Todos().Create)
		fake.GET("/todos/:id", controllers.FakeResource.Todos().Show)
		fake.PUT("/todos/:id", controllers.FakeResource.Todos().Update)
		fake.PATCH("/todos/:id", controllers.FakeResource.Todos().Patch)
		fake.DELETE("/todos/:id", controllers.FakeResource.Todos().Delete)

		// Posts
		fake.GET("/posts", controllers.FakeResource.Posts().List)
		fake.POST("/posts", controllers.FakeResource.Posts().Create)
		fake.GET("/posts/:id", controllers.FakeResource.Posts().Show)
		fake.PUT("/posts/:id", controllers.FakeResource.Posts().Update)
		fake.PATCH("/posts/:id", controllers.FakeResource.Posts().Patch)
		fake.DELETE("/posts/:id", controllers.FakeResource.Posts().Delete)
		fake.GET("/posts/:id/comments", controllers.FakeResource.Posts().Comments)

		// Comments
		fake.GET("/comments", controllers.FakeResource.Comments().List)
		fake.POST("/comments", controllers.FakeResource.Comments().Create)
		fake.GET("/comments/:id", controllers.FakeResource.Comments().Show)
		fake.PUT("/comments/:id", controllers.FakeResource.Comments().Update)
		fake.PATCH("/comments/:id", controllers.FakeResource.Comments().Patch)
		fake.DELETE("/comments/:id", controllers.FakeResource.Comments().Delete)

		// Albums
		fake.GET("/albums", controllers.FakeResource.Albums().List)
		fake.POST("/albums", controllers.FakeResource.Albums().Create)
		fake.GET("/albums/:id", controllers.FakeResource.Albums().Show)
		fake.PUT("/albums/:id", controllers.FakeResource.Albums().Update)
		fake.PATCH("/albums/:id", controllers.FakeResource.Albums().Patch)
		fake.DELETE("/albums/:id", controllers.FakeResource.Albums().Delete)
		fake.GET("/albums/:id/photos", controllers.FakeResource.Albums().Photos)

		// Photos
		fake.GET("/photos", controllers.FakeResource.Photos().List)
		fake.POST("/photos", controllers.FakeResource.Photos().Create)
		fake.GET("/photos/:id", controllers.FakeResource.Photos().Show)
		fake.PUT("/photos/:id", controllers.FakeResource.Photos().Update)
		fake.PATCH("/photos/:id", controllers.FakeResource.Photos().Patch)
		fake.DELETE("/photos/:id", controllers.FakeResource.Photos().Delete)
	}

	return nil
}
