package context

import (
	"os"

	"github.com/IncSW/geoip2"
)

func NewCore() (*Core, error) {
	file, err := os.ReadFile("GeoIP2-City.mmdb")
	if err != nil {
		return nil, err
	}

	geoipReader, err := geoip2.NewCityReader(file)
	if err != nil {
		return nil, err
	}

	return &Core{
		GeoIP: geoipReader,
	}, nil
}

type Core struct {
	GeoIP *geoip2.CityReader
}
