package context

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/evolves-fr/gommon"
)

const FallbackOutput = "json"

type Echo struct {
	echo.Context
	*Core
}

func (e *Echo) Accept() []string {
	var accepted = make([]string, 0)

	for _, values := range e.Request().Header.Values("Accept") {
		for _, value := range strings.Split(values, ",") {
			for _, item := range strings.Split(value, ";") {
				if !strings.Contains(item, "=") {
					accepted = append(accepted, item)
				}
			}
		}
	}

	return accepted
}

var ContentTypes = map[string]string{
	"json": "application/json",
	"xml":  "application/xml",
}

func (e *Echo) OutputFormat() string {
	var accept = e.Accept()

	if len(accept) == 0 {
		return ""
	}

	switch {
	case gommon.Has("application/json", accept...):
		return "json"
	//case gommon.Has("application/xml", accept...):
	//	return "xml"
	//case gommon.Has("text/csv", accept...):
	//	return "csv"
	//case gommon.Has("text/html", accept...):
	//	return "html"
	//case gommon.Has("text/plain", accept...):
	//	return "txt"
	default:
		return FallbackOutput
	}
}

func (e *Echo) Send(code int, contentType, name string, data []byte) error {
	e.Response().Header().Set(echo.HeaderContentDisposition, fmt.Sprintf("%s; filename=%q", "attachment", name))

	return e.Blob(code, contentType, data)
}

func (e *Echo) Object(code int, v any) error {
	var buffer bytes.Buffer

	var encoder interface {
		Encode(v any) error
	}

	var format = e.OutputFormat()

	// Select encoder from header `Accept`
	switch format {
	case "json":
		encoder = json.NewEncoder(&buffer)
	case "xml":
		encoder = xml.NewEncoder(&buffer)
	default:
		return echo.NewHTTPError(http.StatusNotAcceptable, "Accept='"+format+"'")
	}

	if err := encoder.Encode(v); err != nil {
		return err
	}

	return e.Blob(code, ContentTypes[format], buffer.Bytes())
}
