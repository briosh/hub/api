package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/v1/docs"
	"gitlab.com/evolves-fr/gommon"
	"pkg.brio.sh/swaggerui/v4"
)

var DocResource = docResource{}

type docResource struct{}

func (docResource) Swagger(ctx echo.Context) error {
	swagger := *docs.SwaggerInfo
	swagger.Schemes = append(swagger.Schemes, gommon.Ternary(ctx.Request().TLS == nil, "http", "https"))
	swagger.Host = ctx.Request().Host
	return ctx.Blob(http.StatusOK, "application/json; charset=UTF-8", []byte(swagger.ReadDoc()))
}

func (docResource) SwaggerUI(ctx echo.Context) error {
	swaggerui.Handler("/v1/doc.json").ServeHTTP(ctx.Response(), ctx.Request())
	return nil
}
