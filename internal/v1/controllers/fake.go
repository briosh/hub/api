package controllers

import (
	"gitlab.com/briosh/hub/api/internal/v1/models"
	"gitlab.com/briosh/hub/api/internal/v1/services"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/global/errors"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/evolves-fr/gommon"
)

// TODO: add pagination

var FakeResource = fakeResource{}

type fakeResource struct{}

func (fakeResource) Users() *fakeUsersResource { return &fakeUsersResource{} }

func (fakeResource) Todos() *fakeTodosResource { return &fakeTodosResource{} }

func (fakeResource) Posts() *fakePostsResource { return &fakePostsResource{} }

func (fakeResource) Comments() *fakeCommentsResource { return &fakeCommentsResource{} }

func (fakeResource) Albums() *fakeAlbumsResource { return &fakeAlbumsResource{} }

func (fakeResource) Photos() *fakePhotosResource { return &fakePhotosResource{} }

type fakeUsersResource struct{}

// List users
// @Tags			Fake
// @Summary			Listing all users
// @Accept			json
// @Produce			json
// @Success			200					{array}		models.FakeUser
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users [get]
func (fakeUsersResource) List(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	return cc.Object(http.StatusOK, services.Fake().GetUsers())
}

// Show user
// @Tags			Fake
// @Summary			Get user by id
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int						true 	"User ID"
// @Success			200					{object}	models.FakeUser
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users/{id} [get]
func (fakeUsersResource) Show(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	user, err := services.Fake().GetUser(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, user)
}

// Create user
// @Tags			Fake
// @Summary			Create a new user
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param 			json 				body 		models.FakeUser			true 	"Schema"
// @Success			200					{object}	models.FakeUser
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users [post]
func (fakeUsersResource) Create(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	var user models.FakeUser
	if err := cc.Bind(&user); err != nil {
		return errors.ErrBadRequest
	}

	return cc.Object(http.StatusOK, services.Fake().CreateUser(user))
}

// Update user
// @Tags			Fake
// @Summary			Update user
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int						true 	"User ID"
// @Param 			json 				body 		models.FakeUser			true 	"Schema"
// @Success			200					{object}	models.FakeUser
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users/{id} [put]
func (fakeUsersResource) Update(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeUser
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	user, err := services.Fake().UpdateUser(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, user)
}

// Patch user
// @Tags			Fake
// @Summary			Patch user
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int						true 	"User ID"
// @Param 			json 				body 		models.FakeUser			true 	"Schema"
// @Success			200					{object}	models.FakeUser
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users/{id} [patch]
func (fakeUsersResource) Patch(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeUser
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	user, err := services.Fake().PatchUser(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, user)
}

// Delete user
// @Tags			Fake
// @Summary			Delete user
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"User ID"
// @Success			204
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users/{id} [delete]
func (fakeUsersResource) Delete(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	if err = services.Fake().DeleteUser(id); err != nil {
		return err
	}

	return cc.NoContent(http.StatusNoContent)
}

// Albums of user
// @Tags			Fake
// @Summary			Listing all albums of user
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"User ID"
// @Success			200					{array}		models.FakeAlbum
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users/{id}/albums [get]
func (fakeUsersResource) Albums(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	albums, err := services.Fake().GetUserAlbums(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, albums)
}

// Todos of user
// @Tags			Fake
// @Summary			Listing all todos of user
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"User ID"
// @Success			200					{array}		models.FakeTodo
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users/{id}/todos [get]
func (fakeUsersResource) Todos(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	todos, err := services.Fake().GetUserTodos(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, todos)
}

// Posts of user
// @Tags			Fake
// @Summary			Listing all ports of user
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"User ID"
// @Success			200					{array}		models.FakePost
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/users/{id}/posts [get]
func (fakeUsersResource) Posts(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	posts, err := services.Fake().GetUserPosts(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, posts)
}

type fakeTodosResource struct{}

// List tasks
// @Tags			Fake
// @Summary			Listing all tasks
// @Accept			json
// @Produce			json
// @Success			200					{array}		models.FakeTodo
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/todos [get]
func (fakeTodosResource) List(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	return cc.Object(http.StatusOK, services.Fake().GetTodos())
}

// Show task
// @Tags			Fake
// @Summary			Get task by id
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Task ID"
// @Success			200					{object}	models.FakeTodo
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/todos/{id} [get]
func (fakeTodosResource) Show(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	user, err := services.Fake().GetTodo(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, user)
}

// Create task
// @Tags			Fake
// @Summary			Create a new task
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param 			json 				body 		models.FakeTodo			true 	"Schema"
// @Success			200					{object}	models.FakeTodo
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/todos [post]
func (fakeTodosResource) Create(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	var req models.FakeTodo
	if err := cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	todo, err := services.Fake().CreateTodo(req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, todo)
}

// Update task
// @Tags			Fake
// @Summary			Update task
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int						true 	"Task ID"
// @Param 			json 				body 		models.FakeUser			true 	"Schema"
// @Success			200					{object}	models.FakeTodo
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/todos/{id} [put]
func (fakeTodosResource) Update(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeTodo
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	user, err := services.Fake().UpdateTodo(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, user)
}

// Patch task
// @Tags			Fake
// @Summary			Patch task
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int						true 	"Task ID"
// @Param 			json 				body 		models.FakeUser			true 	"Schema"
// @Success			200					{object}	models.FakeTodo
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/todos/{id} [patch]
func (fakeTodosResource) Patch(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeTodo
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	user, err := services.Fake().PatchTodo(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, user)
}

// Delete task
// @Tags			Fake
// @Summary			Delete task
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Task ID"
// @Success			200					{object}	models.FakeTodo
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/todos/{id} [delete]
func (fakeTodosResource) Delete(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	if err = services.Fake().DeleteTodo(id); err != nil {
		return err
	}

	return cc.NoContent(http.StatusNoContent)
}

type fakePostsResource struct{}

// List posts
// @Tags			Fake
// @Summary			Listing all posts
// @Accept			json
// @Produce			json
// @Success			200					{array}		models.FakePost
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/posts [get]
func (fakePostsResource) List(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	return cc.Object(http.StatusOK, services.Fake().GetPosts())
}

// Show post
// @Tags			Fake
// @Summary			Get post by id
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Post ID"
// @Success			200					{object}	models.FakePost
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/posts/{id} [get]
func (fakePostsResource) Show(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	post, err := services.Fake().GetPost(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, post)
}

// Create post
// @Tags			Fake
// @Summary			Create a new post
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.FakePost
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/posts [post]
func (fakePostsResource) Create(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	var req models.FakePost
	if err := cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	post, err := services.Fake().CreatePost(req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, post)
}

// Update post
// @Tags			Fake
// @Summary			Update post
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Post ID"
// @Success			200					{object}	models.FakePost
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/posts/{id} [put]
func (fakePostsResource) Update(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakePost
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	post, err := services.Fake().UpdatePost(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, post)
}

// Patch post
// @Tags			Fake
// @Summary			Patch post
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Post ID"
// @Success			200					{object}	models.FakePost
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/posts/{id} [patch]
func (fakePostsResource) Patch(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakePost
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	post, err := services.Fake().PatchPost(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, post)
}

// Delete post
// @Tags			Fake
// @Summary			Delete post
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Post ID"
// @Success			200					{object}	models.FakePost
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/posts/{id} [delete]
func (fakePostsResource) Delete(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	if err = services.Fake().DeletePost(id); err != nil {
		return err
	}

	return cc.NoContent(http.StatusNoContent)
}

// Comments of post
// @Tags			Fake
// @Summary			Listing all comments of post
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Post ID"
// @Success			200					{array}		models.FakeComment
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/posts/{id}/comments [get]
func (fakePostsResource) Comments(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	comments, err := services.Fake().GetPostComments(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, comments)
}

type fakeCommentsResource struct{}

// List comments
// @Tags			Fake
// @Summary			Listing all comments
// @Accept			json
// @Produce			json
// @Success			200					{array}		models.FakeComment
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/comments [get]
func (fakeCommentsResource) List(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	return cc.Object(http.StatusOK, services.Fake().GetComments())
}

// Show comment
// @Tags			Fake
// @Summary			Get comment by id
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Comment ID"
// @Success			200					{object}	models.FakeComment
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/comments/{id} [get]
func (fakeCommentsResource) Show(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	comment, err := services.Fake().GetComment(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, comment)
}

// Create comment
// @Tags			Fake
// @Summary			Create a new comment
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.FakeComment
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/comments [post]
func (fakeCommentsResource) Create(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	var req models.FakeComment
	if err := cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	comment, err := services.Fake().CreateComment(req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, comment)
}

// Update comment
// @Tags			Fake
// @Summary			Update comment
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Comment ID"
// @Success			200					{object}	models.FakeComment
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/comments/{id} [put]
func (fakeCommentsResource) Update(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeComment
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	comment, err := services.Fake().UpdateComment(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, comment)
}

// Patch comment
// @Tags			Fake
// @Summary			Patch comment
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Comment ID"
// @Success			200					{object}	models.FakeComment
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/comments/{id} [patch]
func (fakeCommentsResource) Patch(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeComment
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	comment, err := services.Fake().PatchComment(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, comment)
}

// Delete comment
// @Tags			Fake
// @Summary			Delete comment
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Comment ID"
// @Success			200					{object}	models.FakeComment
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/comments/{id} [delete]
func (fakeCommentsResource) Delete(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	if err = services.Fake().DeleteComment(id); err != nil {
		return err
	}

	return cc.NoContent(http.StatusNoContent)
}

type fakeAlbumsResource struct{}

// List albums
// @Tags			Fake
// @Summary			Listing all albums
// @Accept			json
// @Produce			json
// @Success			200					{array}		models.FakeAlbum
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/albums [get]
func (fakeAlbumsResource) List(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	return cc.Object(http.StatusOK, services.Fake().GetAlbums())
}

// Show album
// @Tags			Fake
// @Summary			Get album by id
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Album ID"
// @Success			200					{object}	models.FakeAlbum
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/albums/{id} [get]
func (fakeAlbumsResource) Show(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	album, err := services.Fake().GetAlbum(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, album)
}

// Create album
// @Tags			Fake
// @Summary			Create a new album
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.FakeAlbum
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/albums [post]
func (fakeAlbumsResource) Create(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	var req models.FakeAlbum
	if err := cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	album, err := services.Fake().CreateAlbum(req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, album)
}

// Update album
// @Tags			Fake
// @Summary			Update album
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Album ID"
// @Success			200					{object}	models.FakeAlbum
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/albums/{id} [put]
func (fakeAlbumsResource) Update(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeAlbum
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	album, err := services.Fake().UpdateAlbum(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, album)
}

// Patch album
// @Tags			Fake
// @Summary			Patch album
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Album ID"
// @Success			200					{object}	models.FakeAlbum
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/albums/{id} [patch]
func (fakeAlbumsResource) Patch(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakeAlbum
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	album, err := services.Fake().PatchAlbum(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, album)
}

// Delete album
// @Tags			Fake
// @Summary			Delete album
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Album ID"
// @Success			200					{object}	models.FakeAlbum
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/albums/{id} [delete]
func (fakeAlbumsResource) Delete(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	if err = services.Fake().DeleteAlbum(id); err != nil {
		return err
	}

	return cc.NoContent(http.StatusNoContent)
}

// Photos of album
// @Tags			Fake
// @Summary			Listing all photos of album
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Album ID"
// @Success			200					{array}		models.FakePhoto
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/albums/{id}/photos [get]
func (fakeAlbumsResource) Photos(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	photos, err := services.Fake().GetAlbumPhotos(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, photos)
}

type fakePhotosResource struct{}

// List photos
// @Tags			Fake
// @Summary			Listing all photos
// @Accept			json
// @Produce			json
// @Success			200					{array}		models.FakePhoto
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/photos [get]
func (fakePhotosResource) List(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	return cc.Object(http.StatusOK, services.Fake().GetPhotos())
}

// Show photo
// @Tags			Fake
// @Summary			Get photo by id
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Photo ID"
// @Success			200					{object}	models.FakePhoto
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/photos/{id} [get]
func (fakePhotosResource) Show(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	photo, err := services.Fake().GetPhoto(id)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, photo)
}

// Create photo
// @Tags			Fake
// @Summary			Create a new photo
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.FakePhoto
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/photos [post]
func (fakePhotosResource) Create(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	var req models.FakePhoto
	if err := cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	photo, err := services.Fake().CreatePhoto(req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, photo)
}

// Update photo
// @Tags			Fake
// @Summary			Update photo
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Photo ID"
// @Success			200					{object}	models.FakePhoto
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/photos/{id} [put]
func (fakePhotosResource) Update(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakePhoto
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	photo, err := services.Fake().UpdatePhoto(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, photo)
}

// Patch photo
// @Tags			Fake
// @Summary			Patch photo
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Photo ID"
// @Success			200					{object}	models.FakePhoto
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/photos/{id} [patch]
func (fakePhotosResource) Patch(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	var req models.FakePhoto
	if err = cc.Bind(&req); err != nil {
		return errors.ErrBadRequest
	}

	photo, err := services.Fake().PatchPhoto(id, req)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, photo)
}

// Delete photo
// @Tags			Fake
// @Summary			Delete photo
// @Description		Important: resource will not be really updated on the server but it will be faked as if.
// @Accept			json
// @Produce			json
// @Param   		id 					path 		int		true 	"Photo ID"
// @Success			200					{object}	models.FakePhoto
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/fake/photos/{id} [delete]
func (fakePhotosResource) Delete(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	id, err := gommon.Parse[uint32](cc.Param("id"))
	if err != nil {
		return errors.ErrBadRequest
	}

	if err = services.Fake().DeletePhoto(id); err != nil {
		return err
	}

	return cc.NoContent(http.StatusNoContent)
}
