package controllers

import (
	"fmt"
	"io"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/briosh/hub/api/internal/v1/services"
	"gitlab.com/evolves-fr/gommon"
)

var ImageResource = imagesResource{}

type imagesResource struct{}

// Convert image to another format
// @Tags			Images
// @Summary			Image converter
// @Description		Convert image to another format
// @Accept			multipart/form-data
// @Produce			image/png
// @Produce			image/jpeg
// @Produce			image/webp
// @Param			file				formData	file	true	"image"
// @Success			200
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/images/convert [post]
func (imagesResource) Convert(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	// Get output format
	contentType := cc.Request().Header.Get("Accept")

	// Get extension format
	extension, err := services.ImageExtension(contentType)
	if err != nil {
		return err
	}

	// Source upload
	form, err := ctx.FormFile("file")
	if err != nil {
		return err
	}

	// Open source file
	file, err := form.Open()
	if err != nil {
		return err
	}

	// Read source file
	source, err := io.ReadAll(file)
	if err != nil {
		return err
	}

	// Close source file
	if err = file.Close(); err != nil {
		return err
	}

	// Convert image
	output, err := services.ImageConvert(source, extension)
	if err != nil {
		return err
	}

	// Output name
	outputName := strings.TrimSuffix(form.Filename, filepath.Ext(form.Filename)) + extension

	return cc.Send(http.StatusOK, contentType, outputName, output)

}

// Placeholder image
// @Tags			Images
// @Summary			Placeholder image
// @Description		Placeholder image
// @Produce			image/svg+xml
// @Produce			image/png
// @Produce			image/jpeg
// @Produce			image/webp
// @Param			size				path		string	true	"Size"
// @Param			text				query		string	false	"Text value"
// @Param			backgroundColor		query		string	false	"Background color" 	default(#303030)
// @Param			textColor			query		string	false	"Text color" 		default(#FFFFFF)
// @Success			200
// @Failure			400 				{object} 	errors.Error
// @Failure			404 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/images/placeholder/{size} [get]
func (imagesResource) Placeholder(ctx echo.Context) error {
	size := strings.Split(ctx.Param("size"), "x")

	width, err := gommon.Parse[int](size[0])
	if err != nil {
		return err
	}

	height, err := gommon.Parse[int](size[1])
	if err != nil {
		return err
	}

	var (
		text            = gommon.Default(ctx.QueryParam("text"), fmt.Sprintf("%dx%d", width, height))
		backgroundColor = gommon.Default(ctx.QueryParam("backgroundColor"), "#303030")
		textColor       = gommon.Default(ctx.QueryParam("textColor"), "#FFFFFF")
	)

	svg := fmt.Sprintf(`<svg width="%d" height="%d" viewBox="0 0 %d %d" fill="none" xmlns="http://www.w3.org/2000/svg">
	<rect width="%d" height="%d" fill="%s"/>
	<text x="%d" y="%d" fill="%s" alignment-baseline="middle" text-anchor="middle" font-size="24">%s</text>
</svg>
`,
		width, height, width, height, width, height, backgroundColor, width/2, height/2, textColor, text,
	)

	return ctx.Blob(http.StatusOK, "image/svg+xml", []byte(svg))
}
