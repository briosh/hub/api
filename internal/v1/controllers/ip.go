package controllers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/briosh/hub/api/internal/v1/services"
	"net/http"
	"net/url"
)

type IPResource struct{}

// GeoMe ip information
// @Tags			IP
// @Summary			Get IP information with geolocation for me
// @Accept			json
// @Produce			json
// @Success			200					{array}		models.GeoIP
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/ip/geo [get]
func (IPResource) GeoMe(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	geoip, err := services.FindGeoIP(cc.Core, cc.RealIP(), cc.QueryParam("lang"))
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, geoip)
}

// GeoSearch ip information
// @Tags			IP
// @Summary			Get IP information with geolocation
// @Accept			json
// @Produce			json
// @Param   		ip 					path 		string		true 	"IPV4 or IPV6 address"
// @Success			200					{array}		models.GeoIP
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/ip/geo/{ip} [get]
func (IPResource) GeoSearch(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	geoip, err := services.FindGeoIP(cc.Core, cc.Param("ip"), cc.QueryParam("lang"))
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, geoip)
}

// Compute ip information
// @Tags			IP
// @Summary			Calculate IP network
// @Accept			json
// @Produce			json
// @Param   		ip 					path 		string		true 	"IPV4 or IPV6 address"
// @Param   		mask				path 		string		true 	"Network mask"
// @Success			200					{object}	models.ComputeIP
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/ip/compute/{ip}/{mask} [get]
func (IPResource) Compute(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	ip := cc.Param("ip")
	if ipDecoded, err := url.QueryUnescape(cc.Param("ip")); err == nil {
		ip = ipDecoded
	}

	geoip, err := services.ComputeIP(cc.Core, ip, cc.Param("mask"))
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, geoip)
}
