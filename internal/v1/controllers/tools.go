package controllers

import (
	"io"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/briosh/hub/api/internal/v1/services"
	"gitlab.com/evolves-fr/gommon"
)

var ToolsResource = toolsResource{}

type toolsResource struct{}

// Hash generator
// @Tags			Tools
// @Summary			Hash generator
// @Description		Calculate MD5, SHA1, SHA256 and SHA512 hash from text data.
// @Accept			json
// @Produce			json
// @Param 			value				body 		string			true 	"Body"
// @Param 			toUpper 			query 		bool			false 	"Upper" default(false)
// @Success			200					{object}	models.Hash
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/tools/hash [post]
func (toolsResource) Hash(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	body, err := io.ReadAll(ctx.Request().Body)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, services.Hash(body, gommon.MustParse[bool](cc.QueryParam("toUpper"))))
}

// Number base converter
// @Tags			Tools
// @Summary			Number base converter
// @Description		Convert numbers from one base to another.
// @Accept			json
// @Produce			json
// @Param 			value 				query 		string			true 	"Number"
// @Param 			base 				query 		int				true 	"Base"
// @Success			200					{object}	models.Hash
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/tools/number [post]
func (toolsResource) Number(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	base, err := gommon.Parse[int](cc.QueryParam("base"))
	if err != nil {
		return err
	}

	numbers, err := services.Number(cc.QueryParam("value"), base)
	if err != nil {
		return err
	}

	return cc.Object(http.StatusOK, numbers)
}
