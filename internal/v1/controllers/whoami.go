package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/briosh/hub/api/internal/v1/services"
)

type WhoamiResources struct{}

func (WhoamiResources) Any(ctx echo.Context) error {
	cc := ctx.(*context.Echo)

	whoami, err := services.FindWhoami(cc.Core, ctx)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return cc.Object(http.StatusOK, whoami)
}

// @Tags			Whoami
// @Summary			Get request information
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.Whoami
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/whoami [get]
func (WhoamiResources) _() {}

// @Tags			Whoami
// @Summary			Get request information
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.Whoami
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/whoami [post]
func (WhoamiResources) _() {}

// @Tags			Whoami
// @Summary			Get request information
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.Whoami
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/whoami [put]
func (WhoamiResources) _() {}

// @Tags			Whoami
// @Summary			Get request information
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.Whoami
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/whoami [patch]
func (WhoamiResources) _() {}

// @Tags			Whoami
// @Summary			Get request information
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.Whoami
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/whoami [delete]
func (WhoamiResources) _() {}

// @Tags			Whoami
// @Summary			Get request information
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.Whoami
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/whoami [head]
func (WhoamiResources) _() {}

// @Tags			Whoami
// @Summary			Get request information
// @Accept			json
// @Produce			json
// @Success			200					{object}	models.Whoami
// @Failure			400 				{object} 	errors.Error
// @Failure			500 				{object} 	errors.Error
// @Failure			default 			{object} 	errors.Error
// @Router			/whoami [options]
func (WhoamiResources) _() {}
