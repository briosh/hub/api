package middlewares

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/v1/context"
)

func Context(c *context.Core) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			return next(&context.Echo{
				Context: ctx,
				Core:    c,
			})
		}
	}
}
