package models

import "encoding/xml"

type HTTPHeader map[string][]string

func (h HTTPHeader) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	type Header struct {
		XMLName xml.Name
		Values  []string `xml:"value"`
	}

	if err := e.EncodeToken(start); err != nil {
		return err
	}

	for key, values := range h {
		if err := e.Encode(Header{XMLName: xml.Name{Local: key}, Values: values}); err != nil {
			return err
		}
	}

	return e.EncodeToken(start.End())
}
