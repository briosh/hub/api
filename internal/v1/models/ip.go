package models

import "encoding/xml"

type GeoIP struct {
	XMLName       xml.Name     `json:"-" xml:"geoip"`
	Address       string       `json:"address" xml:"address"`
	EuropeanUnion bool         `json:"european_union" xml:"european_union"`
	Continent     GeoIpValue   `json:"continent" xml:"continent"`
	Country       GeoIpValue   `json:"country" xml:"country"`
	Subdivisions  []GeoIpValue `json:"subdivisions" xml:"subdivisions"`
	City          GeoIpValue   `json:"city" xml:"city"`
	Latitude      float64      `json:"latitude" xml:"latitude"`
	Longitude     float64      `json:"longitude" xml:"longitude"`
	TimeZone      string       `json:"timeZone" xml:"timeZone"`
}

type GeoIpValue struct {
	Code string `json:"code,omitempty" xml:"code,omitempty"`
	Name string `json:"name,omitempty" xml:"name,omitempty"`
}

type ComputeIP struct {
	Address   string `json:"address,omitempty"`
	Broadcast string `json:"broadcast,omitempty"`
	Netmask   string `json:"netmask,omitempty"`
	Wildcard  string `json:"wildcard,omitempty"`
	FirstIP   string `json:"first_ip,omitempty"`
	LastIP    string `json:"last_ip,omitempty"`
	TotalIP   uint64 `json:"total_ip,omitempty"`
}
