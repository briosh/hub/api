package models

type Hash struct {
	MD5    string `json:"md5"`
	SHA1   string `json:"sha1"`
	SHA256 string `json:"sha256"`
	SHA512 string `json:"sha512"`
}

type Number struct {
	Binary      string `json:"binary"`
	Octal       string `json:"octal"`
	Decimal     string `json:"decimal"`
	Hexadecimal string `json:"hexadecimal"`
}
