package models

import "encoding/xml"

type Whoami struct {
	XMLName    xml.Name   `json:"-" xml:"whoami"`
	URL        string     `json:"url" xml:"url"`
	Host       string     `json:"host" xml:"host"`
	RequestURI string     `json:"request_uri" xml:"request_uri"`
	Method     string     `json:"method" xml:"method"`
	Protocol   string     `json:"protocol" xml:"protocol"`
	Remote     string     `json:"remote" xml:"remote"`
	Address    string     `json:"address" xml:"address"`
	Reverse    string     `json:"reverse" xml:"reverse"`
	Geo        *GeoIP     `json:"geo,omitempty" xml:"geo,omitempty"`
	Header     HTTPHeader `json:"header" xml:"header" csv:"header" txt:"header"`
	Body       string     `json:"body,omitempty" xml:"body,omitempty"`
}
