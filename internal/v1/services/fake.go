package services

import (
	"gitlab.com/briosh/hub/api/internal/global/errors"
	"gitlab.com/briosh/hub/api/internal/v1/models"
	"gitlab.com/evolves-fr/gommon"
)

func Fake() fake {
	return fake{}
}

type fake struct{}

// Users

func (fake) GetUsers() []models.FakeUser {
	return models.FakeUsers
}

func (fake) GetUser(id uint32) (models.FakeUser, error) {
	for _, user := range models.FakeUsers {
		if user.ID == id {
			return user, nil
		}
	}

	return models.FakeUser{}, errors.ErrNotFound
}

func (fake) CreateUser(req models.FakeUser) models.FakeUser {
	req.ID = 11

	return req
}

func (c fake) UpdateUser(id uint32, req models.FakeUser) (models.FakeUser, error) {
	user, err := c.GetUser(id)
	if err != nil {
		return models.FakeUser{}, err
	}

	req.ID = user.ID
	req.Address = user.Address
	req.Company = user.Company

	return req, nil
}

func (c fake) PatchUser(id uint32, req models.FakeUser) (models.FakeUser, error) {
	user, err := c.GetUser(id)
	if err != nil {
		return models.FakeUser{}, err
	}

	req.ID = user.ID

	if !gommon.Empty(req.Name) {
		req.Name = user.Name
	}

	if !gommon.Empty(req.Username) {
		req.Username = user.Username
	}

	if !gommon.Empty(req.Email) {
		req.Email = user.Email
	}

	if !gommon.Empty(req.Phone) {
		req.Phone = user.Phone
	}

	if !gommon.Empty(req.Website) {
		req.Website = user.Website
	}

	return req, nil
}

func (c fake) DeleteUser(id uint32) error {
	if _, err := c.GetUser(id); err != nil {
		return err
	}

	return nil
}

func (c fake) GetUserAlbums(id uint32) ([]models.FakeAlbum, error) {
	user, err := c.GetUser(id)
	if err != nil {
		return nil, err
	}

	albums := make([]models.FakeAlbum, 0)

	for _, album := range models.FakeAlbums {
		if album.UserID == user.ID {
			albums = append(albums, album)
		}
	}

	return albums, nil
}

func (c fake) GetUserTodos(id uint32) ([]models.FakeTodo, error) {
	user, err := c.GetUser(id)
	if err != nil {
		return nil, err
	}

	todos := make([]models.FakeTodo, 0)

	for _, todo := range models.FakeTodos {
		if todo.UserID == user.ID {
			todos = append(todos, todo)
		}
	}

	return todos, nil
}

func (c fake) GetUserPosts(id uint32) ([]models.FakePost, error) {
	user, err := c.GetUser(id)
	if err != nil {
		return nil, err
	}

	posts := make([]models.FakePost, 0)

	for _, post := range models.FakePosts {
		if post.UserID == user.ID {
			posts = append(posts, post)
		}
	}

	return posts, nil
}

// Todos

func (fake) GetTodos() []models.FakeTodo {
	return models.FakeTodos
}

func (fake) GetTodo(id uint32) (models.FakeTodo, error) {
	for _, todo := range models.FakeTodos {
		if todo.ID == id {
			return todo, nil
		}
	}

	return models.FakeTodo{}, errors.ErrNotFound
}

func (fake) CreateTodo(req models.FakeTodo) (models.FakeTodo, error) {
	req.ID = 201

	return req, nil
}

func (c fake) UpdateTodo(id uint32, req models.FakeTodo) (models.FakeTodo, error) {
	todo, err := c.GetTodo(id)
	if err != nil {
		return models.FakeTodo{}, err
	}

	req.ID = todo.ID

	return req, nil
}

func (c fake) PatchTodo(id uint32, req models.FakeTodo) (models.FakeTodo, error) {
	todo, err := c.GetTodo(id)
	if err != nil {
		return models.FakeTodo{}, err
	}

	req.ID = todo.ID

	if !gommon.Empty(req.Title) {
		req.Title = todo.Title
	}

	if !gommon.Empty(req.Completed) {
		req.Completed = todo.Completed
	}

	return req, nil
}

func (c fake) DeleteTodo(id uint32) error {
	if _, err := c.GetTodo(id); err != nil {
		return err
	}

	return nil
}

// Albums

func (fake) GetAlbums() []models.FakeAlbum {
	return models.FakeAlbums
}

func (fake) GetAlbum(id uint32) (models.FakeAlbum, error) {
	for _, album := range models.FakeAlbums {
		if album.ID == id {
			return album, nil
		}
	}

	return models.FakeAlbum{}, errors.ErrNotFound
}

func (fake) CreateAlbum(req models.FakeAlbum) (models.FakeAlbum, error) {
	req.ID = 101

	return req, nil
}

func (c fake) UpdateAlbum(id uint32, req models.FakeAlbum) (models.FakeAlbum, error) {
	album, err := c.GetAlbum(id)
	if err != nil {
		return models.FakeAlbum{}, err
	}

	req.ID = album.ID

	return req, nil
}

func (c fake) PatchAlbum(id uint32, req models.FakeAlbum) (models.FakeAlbum, error) {
	album, err := c.GetAlbum(id)
	if err != nil {
		return models.FakeAlbum{}, err
	}

	req.ID = album.ID

	if !gommon.Empty(req.Title) {
		req.Title = album.Title
	}

	return req, nil
}

func (c fake) DeleteAlbum(id uint32) error {
	if _, err := c.GetAlbum(id); err != nil {
		return err
	}

	return nil
}

func (c fake) GetAlbumPhotos(id uint32) ([]models.FakePhoto, error) {
	album, err := c.GetAlbum(id)
	if err != nil {
		return nil, err
	}

	photos := make([]models.FakePhoto, 0)

	for _, photo := range models.FakePhotos {
		if photo.AlbumID == album.ID {
			photos = append(photos, photo)
		}
	}

	return photos, nil
}

// Photos

func (fake) GetPhotos() []models.FakePhoto {
	return models.FakePhotos
}

func (fake) GetPhoto(id uint32) (models.FakePhoto, error) {
	for _, photo := range models.FakePhotos {
		if photo.ID == id {
			return photo, nil
		}
	}

	return models.FakePhoto{}, errors.ErrNotFound
}

func (fake) CreatePhoto(req models.FakePhoto) (models.FakePhoto, error) {
	req.ID = 5001

	return req, nil
}

func (c fake) UpdatePhoto(id uint32, req models.FakePhoto) (models.FakePhoto, error) {
	photo, err := c.GetPhoto(id)
	if err != nil {
		return models.FakePhoto{}, err
	}

	req.ID = photo.ID

	return req, nil
}

func (c fake) PatchPhoto(id uint32, req models.FakePhoto) (models.FakePhoto, error) {
	photo, err := c.GetPhoto(id)
	if err != nil {
		return models.FakePhoto{}, err
	}

	req.ID = photo.ID

	if !gommon.Empty(req.Title) {
		req.Title = photo.Title
	}

	if !gommon.Empty(req.URL) {
		req.URL = photo.URL
	}

	if !gommon.Empty(req.ThumbnailURL) {
		req.ThumbnailURL = photo.ThumbnailURL
	}

	return req, nil
}

func (c fake) DeletePhoto(id uint32) error {
	if _, err := c.GetPhoto(id); err != nil {
		return err
	}

	return nil
}

// Posts

func (fake) GetPosts() []models.FakePost {
	return models.FakePosts
}

func (fake) GetPost(id uint32) (models.FakePost, error) {
	for _, post := range models.FakePosts {
		if post.ID == id {
			return post, nil
		}
	}

	return models.FakePost{}, errors.ErrNotFound
}

func (fake) CreatePost(req models.FakePost) (models.FakePost, error) {
	req.ID = 101

	return req, nil
}

func (c fake) UpdatePost(id uint32, req models.FakePost) (models.FakePost, error) {
	post, err := c.GetPost(id)
	if err != nil {
		return models.FakePost{}, err
	}

	req.ID = post.ID

	return req, nil
}

func (c fake) PatchPost(id uint32, req models.FakePost) (models.FakePost, error) {
	post, err := c.GetPost(id)
	if err != nil {
		return models.FakePost{}, err
	}

	req.ID = post.ID

	if !gommon.Empty(req.Title) {
		req.Title = post.Title
	}

	if !gommon.Empty(req.Body) {
		req.Body = post.Body
	}

	return req, nil
}

func (c fake) DeletePost(id uint32) error {
	if _, err := c.GetPost(id); err != nil {
		return err
	}

	return nil
}

func (c fake) GetPostComments(id uint32) ([]models.FakeComment, error) {
	post, err := c.GetPost(id)
	if err != nil {
		return nil, err
	}

	comments := make([]models.FakeComment, 0)

	for _, comment := range models.FakeComments {
		if comment.PostID == post.ID {
			comments = append(comments, comment)
		}
	}

	return comments, nil
}

// Comments

func (fake) GetComments() []models.FakeComment {
	return models.FakeComments
}

func (fake) GetComment(id uint32) (models.FakeComment, error) {
	for _, comment := range models.FakeComments {
		if comment.ID == id {
			return comment, nil
		}
	}

	return models.FakeComment{}, errors.ErrNotFound
}

func (fake) CreateComment(req models.FakeComment) (models.FakeComment, error) {
	req.ID = 101

	return req, nil
}

func (c fake) UpdateComment(id uint32, req models.FakeComment) (models.FakeComment, error) {
	comment, err := c.GetComment(id)
	if err != nil {
		return models.FakeComment{}, err
	}

	req.ID = comment.ID

	return req, nil
}

func (c fake) PatchComment(id uint32, req models.FakeComment) (models.FakeComment, error) {
	comment, err := c.GetComment(id)
	if err != nil {
		return models.FakeComment{}, err
	}

	req.ID = comment.ID

	if !gommon.Empty(req.Name) {
		req.Name = comment.Name
	}

	if !gommon.Empty(req.Body) {
		req.Body = comment.Body
	}

	return req, nil
}

func (c fake) DeleteComment(id uint32) error {
	if _, err := c.GetComment(id); err != nil {
		return err
	}

	return nil
}
