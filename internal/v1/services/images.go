package services

import (
	"fmt"
	"strings"

	"github.com/discord/lilliput"
	"gitlab.com/evolves-fr/gommon"
)

var imageEncodeOptions = map[string]map[int]int{
	".png":  {lilliput.PngCompression: 7},
	".jpeg": {lilliput.JpegQuality: 100},
	".webp": {lilliput.WebpQuality: 100},
}

func ImageConvert(src []byte, extension string) ([]byte, error) {
	// Verify output format
	if !gommon.Has(extension, ".png", ".jpeg", ".webp") {
		return nil, fmt.Errorf("invalid output format (only png, jpeg and webp)")
	}

	// Decode original content
	decoder, err := lilliput.NewDecoder(src)
	if err != nil {
		return nil, err
	}
	defer decoder.Close()

	// Read original content header
	header, err := decoder.Header()
	if err != nil {
		return nil, err
	}

	// Get ready to resize image, using 8192x8192 maximum resize buffer size
	ops := lilliput.NewImageOps(8192)
	defer ops.Close()

	// Create a buffer to store the output image, 50 MB in this case
	output := make([]byte, 50*1024*1024)

	// Set resize and transcode options
	var opts = &lilliput.ImageOptions{
		FileType:             extension,
		Width:                header.Width(),
		Height:               header.Height(),
		ResizeMethod:         lilliput.ImageOpsNoResize,
		NormalizeOrientation: true,
		EncodeOptions:        imageEncodeOptions[extension],
	}

	// Convert
	return ops.Transform(decoder, opts, output)
}

func ImageExtension(mimetype string) (string, error) {
	switch strings.ToLower(mimetype) {
	case "image/png":
		return ".png", nil
	case "image/jpeg":
		return ".jpeg", nil
	case "image/webp":
		return ".webp", nil
	default:
		return "", fmt.Errorf("don't recognize '%s'", mimetype)
	}
}
