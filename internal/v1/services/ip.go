package services

import (
	"fmt"
	"gitlab.com/briosh/hub/api/internal/global/ip"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/briosh/hub/api/internal/v1/models"
	"gitlab.com/evolves-fr/gommon"
	"net"
)

func FindGeoIP(ctx *context.Core, ip, lang string) (*models.GeoIP, error) {
	// Set default language if not set
	lang = gommon.Default(lang, "en")

	// Parse IP address
	parsedIP := net.ParseIP(ip)
	if parsedIP == nil {
		return nil, fmt.Errorf("invalid ip address")
	}

	// Check if ip address is private or loopback
	if parsedIP.IsPrivate() || parsedIP.IsLoopback() {
		return nil, fmt.Errorf("private ip address")
	}

	// Search ip address into database
	data, err := ctx.GeoIP.Lookup(parsedIP)
	if err != nil {
		return nil, err
	}

	// Prepare GeoIP data result
	result := &models.GeoIP{
		Address:       ip,
		EuropeanUnion: data.Country.IsInEuropeanUnion,
		Continent:     models.GeoIpValue{Code: data.Continent.Code, Name: data.Continent.Names[lang]},
		Country:       models.GeoIpValue{Code: data.Country.ISOCode, Name: data.Country.Names[lang]},
		City:          models.GeoIpValue{Name: data.City.Names[lang]},
		Latitude:      data.Location.Latitude,
		Longitude:     data.Location.Longitude,
		TimeZone:      data.Location.TimeZone,
	}
	for _, sd := range data.Subdivisions {
		result.Subdivisions = append(result.Subdivisions, models.GeoIpValue{Code: sd.ISOCode, Name: sd.Names[lang]})
	}

	// Return GeoIP result
	return result, nil
}

func ComputeIP(ctx *context.Core, ipAddress, ipCIDR string) (models.ComputeIP, error) {
	discovered, err := ip.Discover(ipAddress, ipCIDR)
	if err != nil {
		return models.ComputeIP{}, err
	}

	return models.ComputeIP{
		Address:   discovered.Address.String(),
		Broadcast: discovered.Broadcast.String(),
		Netmask:   discovered.Netmask.String(),
		Wildcard:  discovered.Wildcard.String(),
		FirstIP:   discovered.FirstIP.String(),
		LastIP:    discovered.LastIP.String(),
		TotalIP:   discovered.TotalIP,
	}, nil
}
