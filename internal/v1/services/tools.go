package services

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"strconv"
	"strings"

	"gitlab.com/briosh/hub/api/internal/v1/models"
)

func Hash(value []byte, toUpper bool) models.Hash {

	md5Hash := md5.New()
	md5Hash.Write(value)

	sha1Hash := sha1.New()
	sha1Hash.Write(value)

	sha256Hash := sha256.New()
	sha256Hash.Write(value)

	sha512Hash := sha512.New()
	sha512Hash.Write(value)

	hash := models.Hash{
		MD5:    hex.EncodeToString(md5Hash.Sum(nil)),
		SHA1:   hex.EncodeToString(sha1Hash.Sum(nil)),
		SHA256: hex.EncodeToString(sha256Hash.Sum(nil)),
		SHA512: hex.EncodeToString(sha512Hash.Sum(nil)),
	}

	if toUpper {
		hash.MD5 = strings.ToUpper(hash.MD5)
		hash.SHA1 = strings.ToUpper(hash.SHA1)
		hash.SHA256 = strings.ToUpper(hash.SHA256)
		hash.SHA512 = strings.ToUpper(hash.SHA512)
	}

	return hash
}

func Number(value string, base int) (models.Number, error) {
	number, err := strconv.ParseInt(value, base, 64)
	if err != nil {
		return models.Number{}, err
	}

	return models.Number{
		Binary:      strconv.FormatInt(number, 2),
		Octal:       strconv.FormatInt(number, 8),
		Decimal:     strconv.FormatInt(number, 10),
		Hexadecimal: strconv.FormatInt(number, 16),
	}, nil
}
