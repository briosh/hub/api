package services

import (
	"io"
	"net"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/briosh/hub/api/internal/v1/context"
	"gitlab.com/briosh/hub/api/internal/v1/models"
	"gitlab.com/evolves-fr/gommon"
)

func FindWhoami(ctx *context.Core, cc echo.Context) (*models.Whoami, error) {
	// Set primary information from request
	whoami := &models.Whoami{
		URL:        cc.Request().URL.String(),
		Host:       cc.Request().Host,
		RequestURI: cc.Request().RequestURI,
		Method:     cc.Request().Method,
		Protocol:   cc.Request().Proto,
		Remote:     cc.Request().RemoteAddr,
		Address:    cc.RealIP(),
		Header:     models.HTTPHeader(cc.Request().Header),
	}

	// Find geo ip information from ip
	whoami.Geo, _ = FindGeoIP(ctx, cc.RealIP(), cc.QueryParam("lang"))

	// Find reverse dns from ip
	ptr, _ := net.LookupAddr(cc.RealIP())
	whoami.Reverse = gommon.First(ptr)

	// Get request body
	if gommon.Has(cc.Request().Method, http.MethodPost, http.MethodPut, http.MethodPatch) {
		if body, err := io.ReadAll(cc.Request().Body); err == nil {
			whoami.Body = string(body)
		}
	}

	return whoami, nil
}
