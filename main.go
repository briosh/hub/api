package main

//go:generate swag init -d internal/v1,internal/global -g api.go -o internal/v1/docs --ot go

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/briosh/hub/api/internal"
	"gitlab.com/briosh/hub/api/internal/global/info"
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})

	cli.VersionPrinter = func(ctx *cli.Context) { fmt.Printf("%s - %s - %s", info.Version, info.Commit, info.Date) }
}

func main() {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:    "debug",
			Usage:   "Enable debug mode",
			Aliases: []string{"d"},
			EnvVars: []string{"API_DEBUG"},
		},
		&cli.StringFlag{
			Name:    "address",
			Usage:   "Server address with port",
			EnvVars: []string{"API_ADDRESS"},
			Value:   ":9999",
		},
	}

	authors := []*cli.Author{
		{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"},
		{Name: "Vincent LEFORT", Email: "vincent@nivenn.io"},
	}

	app := &cli.App{
		Name:      "hub-api",
		Usage:     "Hub API sever by Briosh",
		Copyright: "Briosh SAS",
		Authors:   authors,
		Version:   info.Version,
		Flags:     flags,
		Before:    before,
		Action:    action,
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func before(ctx *cli.Context) error {
	if ctx.Bool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
	}

	return nil
}

func action(ctx *cli.Context) error {
	return internal.New(ctx.String("address"))
}
